
## a) Crea un repositorio en GitLab o GitHub llamado TUNOMBRE_markdown
![1](imagenes/1.png)



## b)Clona el repositorio en local
## c)Crea en tu repositorio local un documentoREADME.md
## d) Añadir  al  README.md  los  comandos  utilizados  hasta  ahora  y  hacer  un  commit  inicial con el mensaje “Primer commit de TUNOMBRE”
## e) Sube los cambios al repositorio remoto.

![2](imagenes/2.png)






## f)Crear  en  el  repositorio  local  un  fichero  llamado privado.txt.Crear  en  el  repositorio local una carpeta llamada privada.
## g) Realizar  los  cambios  oportunos  para  que  tanto  el  archivo  como  la  carpeta  sean ignorados por git.

![3](imagenes/3.png)


## i) Añade  el fichero tunombre.md en el que se muestreun listado de los módulos en los que estás matriculado.

![4](imagenes/4.png)



## j) Crea un tag llamado v0.1

![5](imagenes/5.png)



## k)Sube los cambios al repositorio remoto

![6](imagenes/6.png)


## m)Por  último,  crea  una  tabla en  el  documento  anterior en  el  que  se muestre  el  nombre de 2 compañeros y su enlace al repositorio en GitLab o GitHub

| Nombre   |  Enlace |
|----------|------:|
| Brendan | https://gitlab.com/brendan.rodriguezfranco  |
| Juan Carlos Rios | https://gitlab.com/juancrafff |









### HOJA 3  


## 1.-Creación de ramas:
    a)Crea la rama rama-TUNOMBRE
    b)Posiciona tu carpeta de trabajo en esta rama


![7](imagenes/7.png)



## 2.-Añade un fichero y crea la rama remota
    c)Crea un fichero llamado despligue.md con únicamente una cabecera DESPLIEGUE DEAPLICACIONES WEB
    d)Haz un commit con el mensaje “Añadiendo el archivo despliegue.md en la rama-TUNOMBRE”
    e)Sube los cambios al repositorio remoto. NOTA: date cuenta que ahora se deberá hacer con el comando git push origin rama-TUNOMBRE

![8](imagenes/8.png)

![9](imagenes/9.png)



## 3.-Haz un merge directo
    f)Posiciónate en la rama master
    g)Haz un merge de la rama-TUNOMBRE en la rama master


![10](imagenes/10.png)




## 4.-Haz un merge con conflicto
    h)En la rama master añade al ficherodespliegue.md una tabla en la que muestres lostemas de la primera evaluación de Despliegue de Aplicaciones Web
    i)Añade los archivos y haz un commit con el mensaje “Añadida primera evaluaciónDespliegue”
    j)Posiciónate ahora en la rama-TUNOMBRE
    k)Escribe en el fichero despliegue.md otra tabla con los temas de la segunda evaluación de Despliegue de Aplicaciones Web
    l)Añade los archivos y haz un commit con el mensaje “Añadida tabla segunda evaluación Despliegue”
    m)Posiciónate otra vez en master y haz un merge con la rama-TUNOMBRE


![11](imagenes/11.png)




![12](imagenes/12.png)
![13](imagenes/13.png)
![14](imagenes/14.png)
![15](imagenes/15.png)
![16](imagenes/16.png)



## 5.-Arreglo del conflicto
    n)Arregla el conflicto editando el fichero despliegue.md y haz un commitcon el mensaje “Finalizado el conflicto de despliegue.md

![17](imagenes/17.png)


## 6.-Tag y borrar la rama
    o)Crea un tag llamado v0.2
    p)Borra la rama-TUNOMBRE

![18](imagenes/18.png)
![19](imagenes/19.png)


## 7.-Documenta todo y finaliza el ejercicio

